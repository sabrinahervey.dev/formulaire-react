import { useEffect, useState } from "react";
import './Form.css';

function Formulaire() {
  const [emailValue, setEmailValue] = useState('');
    const [passwordValue, setPasswordValue] = useState('');
    const [firstNameValue, setFirstNameValue] = useState('');
    const [lastNameValue, setLastNameValue] = useState('');
    const [ageValue, setAgeValue] = useState('');
    

function handleSubmit(event) {
 event.preventDefault();
 alert(`Your email is ${emailValue} and your password is ${passwordValue}. Your first name is ${firstNameValue} and your last name is ${lastNameValue}. You are ${ageValue} years old.`);


 //vérification des champs vides
 if (emailValue === '') {
   alert('Please enter your email.');  
}
if (passwordValue === '') {
   alert('Please enter your password.');  
}
if (firstNameValue === '') {
   alert('Please enter your first name.');  
}
if (lastNameValue === '') {
   alert('Please enter your last name.');  
}
if (ageValue === '') {
    alert('Please enter your age.');  
    } 
}

const [errorMessage, setErrorMessage] = useState('');
//vérification de l'âge après la soumission du formulaire et affichage du message d'erreur
useEffect(() => {
    //si l'age est inférieur à 18 ans, afficher le message d'erreur
    if (ageValue && ageValue < 18) {
        setErrorMessage('You must be 18 years old.');
    } else {
        setErrorMessage('');
    }
}, [ageValue]);
        



function handleEmailChange(event) { 
 setEmailValue(event.target.value);  
}
  

  return (
    <form onSubmit={handleSubmit}>
        <div>
        <label htmlFor="email">Email: </label>
        <input
            type="email" value={emailValue} onChange={handleEmailChange}/>
        </div>
        <div>
        <label htmlFor="password" >Password:{' '} </label>
        <input
            type="password" value={passwordValue} onChange={(event)=> setPasswordValue(event.target.value)}/>
        </div>
        <div>
        <label htmlFor="firstName">First Name: </label>    
            <input
            type="firstName" value={firstNameValue} onChange={(event)=> setFirstNameValue(event.target.value)}/>
        </div>
        <div>
        <label htmlFor="lastName">Last Name:{' '} </label>    
            <input
            type="lastName" value={lastNameValue} onChange={(event) => setLastNameValue(event.target.value)}/>
        </div>
        
        <label htmlFor="age">Age: </label>    
            <input
            type="number" value={ageValue} onChange={(event) => setAgeValue(event.target.value)}/>
        {errorMessage && <div>{errorMessage}</div>}
      <button type="submit">Submit</button>
    </form>
  );
}

export default Formulaire;
